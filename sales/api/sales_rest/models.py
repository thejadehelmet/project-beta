from django.db import models


class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=100, unique=True)
    vin = models.CharField(max_length=17, unique=True)

    def __str__(self):
        return f'auto vin: {self.vin}'

class AutoDetailsVO(models.Model):
    make = models.CharField(max_length=50)
    model = models.CharField(max_length=50)
    vin = models.CharField(max_length=17, unique=True)

class SalesPerson(models.Model):
    name = models.CharField(max_length=100, unique=True)
    employee_number = models.IntegerField(unique=True)

    def __str__(self):
        return f'sales person: {self.name}'

class PotentialCustomer(models.Model):
    name = models.CharField(max_length=100, unique=True)
    address = models.CharField(max_length=200, unique=True)
    phone_number = models.CharField(max_length=11, unique=True)

    def __str__(self):
        return f'potential customer: {self.name}'

class SalesRecord(models.Model):
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="salesrecord",
        on_delete=models.PROTECT
    )
    sales_person = models.ForeignKey(
        SalesPerson,
        related_name="salesrecord",
        on_delete=models.PROTECT
    )
    customer = models.ForeignKey(
        PotentialCustomer,
        related_name="salesrecord",
        on_delete=models.PROTECT
    )
    price = models.PositiveBigIntegerField()
    
    def __str__(self):
       return f'{self.automobile} sold to {self.customere} by {self.sales_person}'
