from django.urls import path
from .views import api_salesperson, api_customer, api_salesrecord, api_salesperson_record, api_salesperson_detail, api_customer_detail, api_salesrecord_detail, api_car_inventory

urlpatterns = [
    path("sales/employees/", api_salesperson, name="api_salesperson"),
    path("sales/employees/<int:pk>/", api_salesperson_detail, name="api_salesperson_detail"),
    path("sales/<int:pk>/employees", api_salesperson_record, name="api_salesperson_record"),
    path("sales/customers/", api_customer, name="api_customer"),
    path("sales/customers/<int:pk>/", api_customer_detail, name="api_customer_detail"),
    path("sales/", api_salesrecord, name="api_salesrecord"),
    path("sales/<int:pk>/", api_salesrecord_detail, name="api_salesrecord_detail"),
    path("sales/cars/", api_car_inventory, name="api_car_inventory"),
    
]
