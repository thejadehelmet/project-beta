from django.http import JsonResponse
import json
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
from .models import AutomobileVO , SalesPerson , PotentialCustomer , SalesRecord 
from .encoders import SalesPersonEncoder, PotentialCustomerEncoder, SalesRecordEncoder, AutomobileVOEncoder


# List all sales persons and create sales person
@require_http_methods(["GET", "POST"])
def api_salesperson(request):
    if request.method == "GET":
        sales_person = SalesPerson.objects.all()
        return JsonResponse(
            {"sales_person": sales_person},
            encoder=SalesPersonEncoder,
        )
    else:
        content = json.loads(request.body)
        sales_person = SalesPerson.objects.create(**content)
        return JsonResponse(
            sales_person,
            encoder=SalesPersonEncoder,
            safe=False,
        )

# List specific sales person details and delete sales person 
@require_http_methods(["GET", "DELETE"])
def api_salesperson_detail(request, pk):
    if request.method == "GET":
        sales_person = SalesPerson.objects.get(id=pk)
        return JsonResponse(
            {"sales_person": sales_person},
            encoder=SalesPersonEncoder,
        )
    else:
        sales_person = SalesPerson.objects.get(id=pk)
        sales_person.delete()
        return JsonResponse(
            sales_person,
            encoder=SalesPersonEncoder,
            safe=False,
            )


# List all potential customers and create potential customers
@require_http_methods(["GET", "POST"])
def api_customer(request):
    if request.method == "GET":
        potential_customer= PotentialCustomer.objects.all()
        return JsonResponse(
            {"potential_customers": potential_customer},
            encoder=PotentialCustomerEncoder
        )
    else:
        content = json.loads(request.body)
        potential_customer = PotentialCustomer.objects.create(**content)
        return JsonResponse(
            potential_customer,
            encoder=PotentialCustomerEncoder,
            safe=False,
        )

# List specific customers detail and delete customer
@require_http_methods(["GET", "DELETE"])
def api_customer_detail(request,pk):
    if request.method == "GET":
        potential_customer= PotentialCustomer.objects.get(id=pk)
        return JsonResponse(
            {"potential_customer": potential_customer},
            encoder=PotentialCustomerEncoder
        )
    else:
        customer = PotentialCustomer.objects.get(id=pk)
        customer.delete()
        return JsonResponse(
            customer,
            encoder=PotentialCustomerEncoder,
            safe=False,
            )

# List all sales records and create sales record
@require_http_methods(["GET", "POST"])
def api_salesrecord(request):
    if request.method == "GET":
        sales= SalesRecord.objects.all()
        return JsonResponse (
            {"sales": sales},
            encoder=SalesRecordEncoder,
        )
    else:
        content = json.loads(request.body)
        print(content)

        try:
            vin = content["automobile"]
            automobile = AutomobileVO.objects.get(vin=vin)
            content["automobile"] = automobile

            person_id = content["sales_person"]
            sales_person = SalesPerson.objects.get(id=person_id)
            content["sales_person"] = sales_person
            
            customer_name = content["customer"]
            customer = PotentialCustomer.objects.get(name=customer_name)
            content["customer"] = customer

            sales_record = SalesRecord.objects.create(**content)
            return JsonResponse(
                sales_record,
                encoder=SalesRecordEncoder,
                safe=False,
        )

        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "customer issue"},
                status=400,
            )

# List specifics of sale record and delte sales record
@require_http_methods(["GET", "DELETE"])
def api_salesrecord_detail(request,pk):
    if request.method == "GET":
        sale_record= SalesRecord.objects.get(id=pk)
        return JsonResponse (
            {"sale_record": sale_record},
            encoder=SalesRecordEncoder,
        )
    else:
        sale_record = SalesRecord.objects.get(id=pk)
        sale_record.delete()
        return JsonResponse(
            sale_record,
            encoder=SalesRecordEncoder,
            safe=False,
            )

# Getting sales record by specific sales person
@require_http_methods(["GET"])
def api_salesperson_record(request, pk):
    if request.method == "GET":
        try:
            sales= SalesRecord.objects.filter(sales_person=pk)
            return JsonResponse(
                {"sales": sales},
                encoder=SalesRecordEncoder,
                safe=False
            )
        except SalesRecord.DoesNotExist:
            return JsonResponse(
                {"message": "No sales records"}, status=404
            )

# Need to get list of cars not sold
@require_http_methods({"GET"})
def api_car_inventory(request):
    sold_cars = []
    for auto in SalesRecord.objects.all():
        sold_cars.append(auto.automobile.vin)
    available = AutomobileVO.objects.exclude(vin__in=sold_cars)
    return JsonResponse(
        {"autos": available}, 
        encoder=AutomobileVOEncoder, 
        safe=False
    )

@require_http_methods({"DELETE"})
def api_car_inventory_delete(request,pk):
    sold = []
    for auto in SalesRecord.objects.all():
        sold.append(auto.automobile.vin)
    available = AutomobileVO.objects.exclude(vin__in=sold)
    return JsonResponse(
        {"autos": available}, 
        encoder=AutomobileVOEncoder, 
        safe=False
    )