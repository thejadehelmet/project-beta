from django.urls import path
from .views import api_list_appointments, api_list_technicians, api_complete_appointment, api_show_technician

urlpatterns = [
    path('appointments/', api_list_appointments, name="api_list_appointments"),
    path('appointments/<int:pk>/', api_complete_appointment, name="api_complete_appointment"),
    path('appointments/?vin=<str:vin>/', api_list_appointments, name="api_list_appointments"),
    path('technicians/', api_list_technicians, name="api_list_technicians"),
    path('technicians/', api_list_technicians, name="api_create_technician"),
    path('technicians/<int:pk>/', api_show_technician, name="api_show_technician"),
    path('technicians/<int:pk>/today', api_show_technician, name="api_show_technician_schedule"),

]