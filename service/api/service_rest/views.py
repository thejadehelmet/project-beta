from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import AutomobileVO, Technician, Appointment


class AutomobileVODetailEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["vin", "import_href"]


class TechnicianDetailEncoder(ModelEncoder):
    model = Technician
    properties = ["name", "employee_number", "pk"]

    def get_extra_data(self, o):
        appointments = o.appointments.all().order_by('-appointment_date')
        appointment_list = []
        for appointment in appointments:
            appointment_dict = {
                "vin": appointment.vin,
                "owner": appointment.owner,
                "date/time": appointment.appointment_date,
                "reason": appointment.reason,
                "is_completed": appointment.is_completed,
                "was_canceled": appointment.was_canceled,
                "vin_is_from_inventory": appointment.vin_is_from_inventory,
                "is_today": appointment.is_today,
                "pk": appointment.pk,
            }
            appointment_list.append(appointment_dict)
        return {"appointments": appointment_list}


class AppointmentDetailEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "vin",
        "owner",
        "appointment_date",
        "technician",
        "reason",
        "is_completed",
        "was_canceled",
        "pk",
    ]
    encoders = {
        "technician": TechnicianDetailEncoder(),
    }

    def get_extra_data(self, o):
        count = AutomobileVO.objects.filter(vin=o.vin).count()
        print(count)

        if count > 0:
            return {"vin_is_from_inventory": True}

        else:
            return {"vin_is_from_inventory": False}



@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    vin = request.GET.get("vin")

    if request.method == "GET":
        if vin is not None:
            appointments = Appointment.objects.filter(vin=vin).order_by("-appointment_date")
        else:
            appointments = Appointment.objects.filter(is_completed=False, was_canceled=False).order_by("-appointment_date")
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentDetailEncoder,
        )
    elif request.method == "POST":
        content = json.loads(request.body)

        try:
            technician = Technician.objects.get(employee_number=content["technician"])
            content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid technician id"},
                status=400,
            )
        conflict = Appointment.objects.filter(technician=technician, appointment_date=content["appointment_date"])
        if len(conflict) == 0:
            appointment = Appointment.objects.create(**content)
            return JsonResponse(
                appointment,
                encoder=AppointmentDetailEncoder,
                safe=False,
            )
        else:
            return JsonResponse(
                {"message": "Technician is already booked for this time"},
                status=409,
            )


@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianDetailEncoder,
        )
    elif request.method == "POST":
        content = json.loads(request.body)
        technician = Technician.objects.create(**content)
        return JsonResponse(
            technician,
            encoder=TechnicianDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET"])
def api_show_technician(request, pk):
    technician = Technician.objects.get(pk=pk)
    return JsonResponse(
        {"technician": technician},
        encoder=TechnicianDetailEncoder,
    )


@require_http_methods(["PUT", "DELETE"])
def api_complete_appointment(request, pk):
    if request.method == "PUT":
        content = json.loads(request.body)
        Appointment.objects.filter(id=pk).update(**content)
        appointment = Appointment.objects.get(id=pk)
        return JsonResponse(
            appointment,
            encoder=AppointmentDetailEncoder,
            safe=False,
        )
    else:
        count, _ = Appointment.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
