# Generated by Django 4.0.3 on 2022-09-12 22:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('service_rest', '0002_rename_appointmentdate_appointment_appointment_date'),
    ]

    operations = [
        migrations.AddField(
            model_name='appointment',
            name='is_completed',
            field=models.BooleanField(default=False),
        ),
    ]
