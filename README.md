# CarCar

Team:

* Shane Price - Sales
* Stacie Herrington - Service

## Design

## Service microservice

My models are Appointment, Technician, and AutomobileVO. Appointment contains the fields VIN, owner, appointment date/time, reason, technician, and three Boolean fields: vin is from inventory (to determine whether the car came from the dealership inventory so that customers can get VIP treatment at service appointment), is completed (to be able to mark an appointment complete), and was canceled (to be able to mark an appointment canceled). (Canceled appointments can then be deleted.) The Technician model includes the fields name and employee number. The AutomobileVO model includes the VIN field and allows integration with the inventory microservice. This is how we are able to check if a car comes from dealership inventory.

## Sales microservice

The sales microservices allows for the creation and tracking of sales records at a car dealership. All dealership sales records can be viewed and can also be filtered down to indivudal sales by each salesperson. A sales record is created by logging the saleperson, customer, vehicle from inventory being sold, and sales price. The sales microservice provides a way to create a salesperson and customer to be used in the sale record form. To get the inventory, the sales microservies polls data from the inventory microservice. When a sales record is created, the vehicle is pulled from the inventory and can not be sold again.