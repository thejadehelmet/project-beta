import React from 'react';

class SaleRecordForm extends React.Component{
  constructor(props) {
    super(props);
    this.state = {
        automobiles: [],
        sales_persons: [],
        customers: [],
        price: '',
    }
}

async componentDidMount() {
  const automobileResponse = await fetch(`http://localhost:8090/api/sales/cars/`);
  const salesPersonResponse = await fetch(`http://localhost:8090/api/sales/employees/`);
  const customerResponse = await fetch(`http://localhost:8090/api/sales/customers/`);

  if (automobileResponse.ok && salesPersonResponse && customerResponse.ok) {
    const automobileData = await automobileResponse.json();
    const salesPersonData = await salesPersonResponse.json();
    const customerData = await customerResponse.json();
    
    this.setState({ automobiles: automobileData.autos },
    this.setState({ sales_persons: salesPersonData.sales_person}),
    this.setState({ customers: customerData.potential_customers })
    );
  }
}

handleChange = event => {
    const {name, value} = event.target;
    this.setState({
        [name]: value
    });
}

handleSubmit = async event => {
  event.preventDefault();
  const data = {...this.state};
  
  delete data.automobiles;
  delete data.customers;
  delete data.sales_persons;

  
  
  const response = await fetch(
    `http://localhost:8090/api/sales/`, 
      { 
      method: "post",
      body: JSON.stringify(data),
      headers: {'Content-Type': 'application/json'}
      }
  );

  if (response.ok) {
    window.location.reload(false);
  }
}

render() {
  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a Sale Record</h1>
          <form onSubmit={this.handleSubmit} id="create-hat-form">
            <div className="mb-3">
              <select onChange={this.handleChange} required name="automobile" id="automobile" className="form-select">
              <option value="">Choose an automobile</option>
                {this.state.automobiles.map(automobile => {
                  return (
                  <option key={automobile.vin} value={automobile.vin}>{automobile.vin}{automobile.model}</option>
                  )
                })}
              </select>
            </div>
            <div className="mb-3">
              <select onChange={this.handleChange} required name="sales_person" id="sales_person" className="form-select">
              <option value="">Choose a sales person</option>
                {this.state.sales_persons.map(salePerson=> {
                  return (
                  <option key={salePerson.id} value={salePerson.id}>{salePerson.name}</option>
                  )
                })}
              </select>
            </div>
            <div className="mb-3">
              <select onChange={this.handleChange} required name="customer" id="customer" className="form-select">
              <option value="">Choose a customer</option>
              {this.state.customers.map(customer=> {
                return (
                <option key={customer.name} value={customer.name}>{customer.name}</option>
                )
              })}
              </select>
            </div>
          <div className="form-floating mb-3">
            <input onChange={this.handleChange} value={this.state.price} placeholder="price" required type="number" name="price" id="price" className="form-control" />
            <label htmlFor="employee_number">Sale price</label>
          </div> 
          <button className="btn bg-success">Create</button>
        </form>
      </div>
    </div>
  </div>
)
}


}



export default SaleRecordForm