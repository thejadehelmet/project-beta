import React from "react";


class SalesRecordList extends React.Component {
  constructor() {
    super();
    this.state = {
      sales: [],
    };
  }

  async componentDidMount() {
    const response = await fetch(`http://localhost:8090/api/sales/`);
    if (response.ok) {
      const data = await response.json();
      this.setState({ sales: data.sales });
    }
  }

  render() {
    return (
        <>
        <br/>
        <h1>Car Sales History</h1>
        <br/>
        <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
        </div>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Sale Person</th>
                    <th>Sales Person Employee # </th>
                    <th>Purchaser Name</th>
                    <th>Automobile VIN</th>
                    <th>Sold Price</th>
                </tr>
            </thead>
            <tbody>
            {this.state.sales.map((sale) =>{
                return(
                    <tr key={sale.id}>
                        <td>{sale.sales_person.name}</td>
                        <td>{sale.sales_person.employee_number}</td>
                        <td>{sale.customer.name}</td>
                        <td>{sale.automobile.vin}</td>
                        <td>${sale.price}</td>
                    </tr>
                )
            })}
            </tbody>
        </table>
        </>
    );
  }
}

export default SalesRecordList;
