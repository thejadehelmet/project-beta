import React from "react";


class SalesPersonRecordList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            sales_persons_list: [],
            sales_person_record: [],
        }
        this.handleChangeSalesPerson = this.handleChangeSalesPerson.bind(this);
    }


    async componentDidMount() {
        const salesPersonResponse = await fetch(`http://localhost:8090/api/sales/employees/`);
      
        if (salesPersonResponse.ok) {
          const salesPersonData = await salesPersonResponse.json();
          this.setState({ sales_persons_list: salesPersonData.sales_person})
        }
      }


    async handleChangeSalesPerson(event) {
        const value = event.target.value;
        const url = `http://localhost:8090/api/sales/${value}/employees`;

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            this.setState({ sales_person_record: data.sales })
        }
    }


    render() {
        return (
            <>
            <div>
                <br />
                <h1>Sales Person History</h1>
                <br />
                <select onChange={this.handleChangeSalesPerson} required name="sales_persons" id="sales_persons" className="form-select">
                    <option value="">Choose a sales person</option>
                    {this.state.sales_persons_list.map(sales_person => {return (<option key = {sales_person.id} value={sales_person.id}>{sales_person.name}</option>)})}
                </select>
                <br />
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Sales Person</th>
                            <th>Customer</th>
                            <th>VIN</th>
                            <th>Sales Price</th>
                        </tr>
                    </thead>
                    <tbody>
                    {this.state.sales_person_record.map((sales_record, index) => {
                        return(
                            <tr style={sales_record} key={index}>
                                <td>{sales_record.sales_person.name}</td>
                                <td>{sales_record.customer.name}</td>
                                <td>{sales_record.automobile.vin}</td>
                                <td>${sales_record.price}</td>
                            </tr>
                            )
                        })}

                    </tbody>
                </table>
            </div>
            </>

            )
        }
    }

export default SalesPersonRecordList;
