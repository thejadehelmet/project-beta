import React from 'react';
import { Link } from 'react-router-dom';

class ManufacturerForm extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
          name: '',

        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChangeName = this.handleChangeName.bind(this);
      }
    
      async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
    
        const manufacturerUrl = `http://localhost:8100/api/manufacturers/`;
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };
        const response = await fetch(manufacturerUrl, fetchConfig);

        if (response.ok) {
          const newManufacturer = await response.json();
          const cleared = {name: ''};
          this.setState(cleared);
        }
      }
    
      handleChangeName(event) {
        const value = event.target.value;
        this.setState({ name: value });
      }
    
    render(){
        return (
            <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Add a Manufacturer</h1>
                <form onSubmit={this.handleSubmit} id="create-hat-form">
                  <div className="form-floating mb-3">
                    <input onChange={this.handleChangeName} value={this.state.name} placeholder="Customer Name" required type="text" name="name" id="name" className="form-control" />
                    <label htmlFor="name">Name</label>
                  </div>
                  <button className="btn bg-success">Add</button>
                </form>
              </div>
            </div>
          </div>
        )

    }
}

export default ManufacturerForm