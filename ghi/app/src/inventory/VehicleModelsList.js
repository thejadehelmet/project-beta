import {useState, useEffect} from 'react';


async function loadModels(setModelsList) {
  const response = await fetch(`http://localhost:8100/api/models/`);
  if (response.ok) {
    const data = await response.json();
    setModelsList(data.models);
  } else {
    console.error(response);
  }
}

function ModelsList(props) {
  const [modelsList, setModelsList] = useState([]);
  useEffect(() => {
    loadModels(setModelsList);
  }, [])


  return (
  <div>
    <h1>Vehicle Models List</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Manufacturer</th>
            <th>Model</th>
            <th>Picture</th>
          </tr>
        </thead>
        <tbody>
          {modelsList.map(model => {
            return (
              <tr key={model.id}>
                <td>{ model.manufacturer.name }</td>
                <td>{model.name}</td>
                <td><img src="{model.picture_url}"></img></td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
    );
  }

  export default ModelsList;