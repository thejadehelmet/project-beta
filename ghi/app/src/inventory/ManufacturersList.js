import {useState, useEffect} from 'react';


async function loadManufacturers(setManufacturersList) {
  const response = await fetch(`http://localhost:8100/api/manufacturers/`);
  if (response.ok) {
    const data = await response.json();
    setManufacturersList(data.manufacturers);
  } else {
    console.error(response);
  }
}

function ManufacturersList(props) {
  const [manufacturersList, setManufacturersList] = useState([]);
  useEffect(() => {
    loadManufacturers(setManufacturersList);
  }, [])


  return (
  <div>
    <h1>Manufacturers List</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Manufacturer</th>
            <th>ID</th>
          </tr>
        </thead>
        <tbody>
          {manufacturersList.map(manufacturer => {
            return (
              <tr key={manufacturer.id}>
                <td>{ manufacturer.name }</td>
                <td>{manufacturer.id}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
    );
  }

  export default ManufacturersList;