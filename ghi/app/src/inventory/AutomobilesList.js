import {useState, useEffect} from 'react';


async function loadAutomobiles(setAutomobilesList) {
  const response = await fetch(`http://localhost:8100/api/automobiles/`);
  if (response.ok) {
    const data = await response.json();
    setAutomobilesList(data.autos);
  } else {
    console.error(response);
  }
}

function AutomobilesList(props) {
  const [automobilesList, setAutomobilesList] = useState([]);
  useEffect(() => {
    loadAutomobiles(setAutomobilesList);
  }, [])


  return (
  <div>
    <h1>Automobiles in Inventory</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Year</th>
            <th>Manufacturer</th>
            <th>Model</th>
            <th>Color</th>
            <th>VIN</th>
          </tr>
        </thead>
        <tbody>
          {automobilesList.map(automobile => {
            return (
              <tr key={automobile.vin}>
                <td>{automobile.year}</td>
                <td>{ automobile.model.manufacturer.name }</td>
                <td>{automobile.model.name}</td>
                <td>{automobile.color}</td>
                <td>{automobile.vin}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
    );
  }

  export default AutomobilesList;