import { useState, useEffect } from 'react';
import { useParams, Link } from 'react-router-dom';

async function loadTechnician(setTechnician, pk) {
  const response = await fetch(`http://localhost:8080/api/technicians/${pk}/`);
  if (response.ok) {
    const data = await response.json();
    setTechnician(data.technician);
    data.technician.appointments.sort((a, b) => {
      const left = a["date/time"]
      const right = b["date/time"]
      if (left < right) {
        return -1;
      }
      if (left > right) {
        return 1;
      }
      return 0;
    })
  } else {
    console.error(response);
  }
}

function ShowToday(props) {
  const params = useParams();
  const [technician, setTechnician] = useState({ "appointments": [] });
  useEffect(() => {
    loadTechnician(setTechnician, params.pk);
  }, [])

  return (
    <div>
      <Link to={`/technicians/`}><p>All Technicians</p></Link>
      <Link to={`/technicians/${params.pk}`}><p>All Appointments</p></Link>
      <h1>Technician: {technician.name}</h1>
      <h5>Employee number: {technician.employee_number}</h5>
      <hr></hr>
      <h4>Today</h4>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Time</th>
            <th>VIN</th>
            <th>Customer</th>
            <th>Reason</th>
          </tr>
        </thead>
        <tbody>
          {technician.appointments.map(appointment => {
            return (
              appointment.is_today &&
              <tr key={appointment.pk}>
                <td>{appointment["date/time"].slice(11, 19)}</td>
                <td>{appointment.vin}</td>
                <td>{appointment.owner}</td>
                <td>{appointment.reason}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

export default ShowToday;