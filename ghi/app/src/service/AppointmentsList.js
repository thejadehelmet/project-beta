import {useState, useEffect} from 'react';
import { Link } from 'react-router-dom';


async function loadAppointments(setAppointmentsList, vin) {
  let query = "";
  if (vin !== undefined && vin !== "") {
    query = `?vin=${vin}`
  }
  const response = await fetch(`http://localhost:8080/api/appointments/${query}`);
  if (response.ok) {
    const data = await response.json();
    setAppointmentsList(data.appointments);
  } else {
    console.error(response);
  }
}

function AppointmentsList(props) {
  const [appointmentsList, setAppointmentsList] = useState([]);
  const [vin, setVin] = useState("");
  useEffect(() => {
    loadAppointments(setAppointmentsList);
  }, [])

  async function completeAppointment(pk) {
    const response = await fetch(`http://localhost:8080/api/appointments/${pk}/`, {
      method: 'PUT',
      headers: {'Content-Type': 'application/json'},
      body: JSON.stringify({is_completed: true})
    })
    if (response.ok) {
      loadAppointments(setAppointmentsList);
    }
  }

  async function cancelAppointment(pk) {
    const response = await fetch(`http://localhost:8080/api/appointments/${pk}/`, {
      method: 'PUT',
      headers: {'Content-Type': 'application/json'},
      body: JSON.stringify({was_canceled: true})
    })
    if (response.ok) {
      loadAppointments(setAppointmentsList);
    }
  }

  async function deleteAppointment(pk) {
    const response = await fetch(`http://localhost:8080/api/appointments/${pk}/`, {method: 'DELETE'});
    if (response.ok) {
      loadAppointments(setAppointmentsList);
    }
  }

  return (
  <div>
    { vin === "" &&
      <div>
        <h1>Active Appointments</h1>
        <h5>For individual complete service history please type or click on VIN.</h5>
      </div>
    }
    { vin !== "" &&
      <div>
        <h1>Service History for VIN {vin}</h1>
        <h5>To view all active appointments please clear search bar.</h5>
      </div>
    }
    <form onSubmit={(event) => {event.preventDefault(); loadAppointments(setAppointmentsList, vin)}} method="GET">
      <div className="input-group rounded">
        <input onChange={(event) => {setVin(event.target.value); if (event.target.value === "") {loadAppointments(setAppointmentsList, "")}}} value={vin} type="search" className="form-control rounded" placeholder="Search by VIN" aria-label="Search" aria-describedby="search-addon" />
        <button className="btn btn-outline-secondary" id="button-addon2">Search</button>
      </div>
    </form>
      <table className="table table-striped align-middle">
        <thead>
          <tr>
            <th>Owner</th>
            <th>Appointment date</th>
            <th>Technician</th>
            <th>Reason</th>
            <th>VIN</th>
            <th>VIP</th>
            <th>Completed</th>
          </tr>
        </thead>
        <tbody>
          {appointmentsList.map(appointment => {
            return (
              <tr key={appointment.pk}>
                <td>{ appointment.owner }</td>
                <td>{appointment.appointment_date.slice(0, 19).replace("T", " at ")}</td>
                <td><Link to={`/technicians/${appointment.technician.pk}`}>{appointment.technician.name}</Link></td>
                <td>{appointment.reason}</td>
                <td><a href="" onClick={(event) =>
                  {setVin(appointment.vin);
                  event.preventDefault();
                  loadAppointments(setAppointmentsList, appointment.vin)}
                  }>{appointment.vin}</a></td>
                <td>{appointment.vin_is_from_inventory &&
                <div>yes</div>
                }
                {!appointment.vin_is_from_inventory &&
                <div>no</div>
                }
                </td>
                <td>
                {!appointment.is_completed && !appointment.was_canceled &&
                  <div>
                    <button className="btn btn-danger btn-sm me-2" onClick={() => cancelAppointment(appointment.pk)}>Cancel</button>
                    <button className="btn btn-primary btn-sm" onClick={() => completeAppointment(appointment.pk)}>Finish</button>
                  </div>
                }
                {appointment.is_completed && !appointment.was_canceled &&
                  <div>
                    completed
                  </div>
                }
                {appointment.was_canceled &&
                  <div>
                    canceled
                  </div>
                }
                </td>
                {appointment.was_canceled &&
                <td>
                  <button className="btn btn-danger btn-sm" onClick={() => deleteAppointment(appointment.pk)}>Delete</button>
                </td>

                }
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
    );
  }

  export default AppointmentsList;