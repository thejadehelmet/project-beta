import {useState, useEffect} from 'react';
import { Link } from 'react-router-dom';


async function loadTechnicians(setTechniciansList) {
    const response = await fetch(`http://localhost:8080/api/technicians`);
    if (response.ok) {
    const data = await response.json();
    setTechniciansList(data.technicians);
  } else {
    console.error(response);
  }
}

function TechniciansList(props) {
  const [techniciansList, setTechniciansList] = useState([]);
  useEffect(() => {
    loadTechnicians(setTechniciansList);
  }, [])

  return (
  <div>
      <h1>Technicians</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
            <th>Employee number</th>
            <th>Today's Appointments</th>
          </tr>
        </thead>
        <tbody>
          {techniciansList.map(technician => {
            return (
              <tr key={technician.pk}>
                <td><Link to={`/technicians/${technician.pk}/`}>{technician.name}</Link></td>
                <td>{technician.employee_number}</td>
                <td><Link to={`/technicians/${technician.pk}/today`}>today</Link></td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
    );
  }

  export default TechniciansList;