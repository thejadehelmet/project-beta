import {useState, useEffect} from 'react';
import {useParams, Link} from 'react-router-dom';

async function loadTechnician(setTechnician, pk) {
    const response = await fetch(`http://localhost:8080/api/technicians/${pk}/`);
    if (response.ok) {
    const data = await response.json();
    setTechnician(data.technician);
  } else {
    console.error(response);
  }
}

function ShowTechnician(props) {
  const params = useParams();
  const [technician, setTechnician] = useState({"appointments": []});
  useEffect(() => {
    loadTechnician(setTechnician, params.pk);
  }, [])

  return (
  <div>
      <Link to={`/technicians/`}><p>All Technicians</p></Link>
      <Link to={`/technicians/${params.pk}/today`}><p>Today's Appointments</p></Link>
      <h1>Technician: {technician.name}</h1>
      <h5>Employee number: {technician.employee_number}</h5>
      <hr></hr>
      <h4>All Appointments</h4>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Date/Time</th>
            <th>VIN</th>
            <th>Customer</th>
            <th>Reason</th>
            <th>Status</th>
          </tr>
        </thead>
        <tbody>
          {technician.appointments.map(appointment => {
            return (
              <tr key={appointment.pk}>
                <td>{appointment["date/time"].slice(0,19).replace("T", " at ")}</td>
                <td>{appointment.vin}</td>
                <td>{appointment.owner}</td>
                <td>{appointment.reason}</td>
                <td>
                {!appointment.is_completed && !appointment.was_canceled &&
                    <div>active</div>
                }
                {appointment.is_completed && !appointment.was_canceled &&
                    <div>completed</div>
                }
                {appointment.was_canceled &&
                    <div>canceled</div>
                }
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
    );
  }

  export default ShowTechnician;